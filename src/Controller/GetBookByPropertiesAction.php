<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\BookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GetBookByPropertiesAction extends AbstractController
{
    public function __invoke(BookRepository $bookRepository): array
    {
        return $bookRepository->findByText();
    }

}


















//    public function __invoke(BookRepository $bookRepository): ?Book
//    {
        // id'si 2 gae teng bo'lgan book'ni topishni buyurdik.
        // Javob sifatida Book sinfiga tegishli obyekt qaytarildi.
        // Agar, bu id'ga ega ma'lumot topilmasa - null qaytadi.

//        return $bookRepository->find(2);
//    }


//        // id'si 2 gae teng bo'lgan book'ni topishni buyurdik.
//        // Javob sifatida Book sinfiga tegishli obyekt qaytarildi.
//        // Agar, bu id'ga ega ma'lumot topilmasa - null qaytadi.
//        $book = $bookRepository->find(2);
//
//        // Ma'lumotlarni shartlar bo'yicha qidirish. SQL'da bu:
//        // WHERE name = 'Deep Work' AND description = 'Rules for
//        // Focused Success in a Distracted World'
//        // LIMIT 1
//        $book = $bookRepository->findOneBy([
//            'name'=> 'Deep Work',
//            'description'=> 'Rules for Focused Success in a Distracted World'
//        ]);
//
//        // Ma'lumotlarni shartlar bo'yicha qidirish. SQL'da bu:
//        // WHERE name = 'Deep Work'
//        // Javob sifatida ko'plik qaytariladi. Har bir ko'plik elementi
//        // Book sinfi obyektiga teng bo'ladi.
//        $book = $bookRepository->findBy(['name'=> 'Deep Work']);
//
//        // O'zimiz yaratgan usullarni ham chaqirishimiz mumkin.
//        $book = $bookRepository->findByText('Deep Work');
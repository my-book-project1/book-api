<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GetCategoryByPropertiesAction extends AbstractController
{
    public function __invoke(CategoryRepository $categoryRepository): ?Category
    {
        return $categoryRepository->findOneBy([
            'id' => 2,
            'name' => 'classic'
        ]);
    }
}
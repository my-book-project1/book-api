<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GetUserByPropertiesAction extends AbstractController
{
    public function __invoke(UserRepository $userRepository) :array
    {
        return $userRepository->findByText('bek');
    }
}
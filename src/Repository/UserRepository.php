<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function save(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return User[] Returns an array of User objects
//     */
//    public function findByText(string $text): array
//    {
//        return $this->createQueryBuilder('u')
//            ->select('u.id', 'u.email')
//            ->andWhere('u.email like :textLike')
//            ->orderBy('u.id', 'ASC')
//            ->setParameter('textLike', '%' . $text . '%')
//            ->setMaxResults(3)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

    public function findMaxAge(): int
    {
        $result = $this->createQueryBuilder('u')
            ->select('max(u.age)')
            ->getQuery()
            ->getSingleScalarResult();

        if ($result === null) {
            return 0;
        }

        return (int)$result;
    }
}
